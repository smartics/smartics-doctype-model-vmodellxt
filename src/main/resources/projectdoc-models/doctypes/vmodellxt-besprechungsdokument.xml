<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-besprechungsdokument"
  base-template="standard"
  provide-type="standard-type"
  category="berichtswesen"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocTimedTitleContextProvider">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Besprechungsdokumente">Besprechungsdokument</name>
      <description>
        Schriftliche Dokumentation des Verlaufs und der Resultate einer Besprechung.
      </description>
      <about>
        Unter dem Besprechungsdokument wird die Dokumentation der
        unterschiedlichen Arten von Besprechungen (wie Jour fixe des Projekts,
        Entwurfsworkshops oder Anforderungserhebungsworkshops) zusammengefasst.
      </about>
      <type plural="Besprechungsdokumenttypen">Besprechungsdokumenttyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-besprechungsdokument-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.vmodellxt-besprechungsdokument.location">
      <resource-bundle>
        <l10n locale="de">
          <name>Ort</name>
          <description>
            Der Ort an dem die Besprechung stattfindet.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-besprechungsdokument.datum">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <resource-bundle>
        <l10n locale="de">
          <name>Datum</name>
          <description>
            Das Datum an dem des Dokument veröffentlicht wurde.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.vmodellxt-besprechungsdokument.from-to">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_common_creationTime"/>]]></xml></value>
      <resource-bundle>
        <l10n locale="de">
          <name>Zeit</name>
          <description>
            Der Start- und Endzeitpunkt der Besprechung.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.vmodellxt-besprechungsdokument.attendees">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder,person</param>
          <param
            name="property"
            key="projectdoc.doctype.vmodellxt-besprechungsdokument.attendees" />
          <param name="empty-as-none">true</param>
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Teilnehmer</name>
          <description>
            Die Liste der Teilnehmer an der Besprechung.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.common.sortKey">
      <value><xml><![CDATA[<at:var at:name="day-date" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Unter dem Besprechungsdokument wird die Dokumentation der
            unterschiedlichen Arten von Besprechungen (wie Jour fixe des
            Projekts, Entwurfsworkshops oder Anforderungserhebungsworkshops)
            zusammengefasst. Dabei wird im Vorfeld eine Einladung verteilt und
            die Besprechung entsprechend dokumentiert. Verantwortlich ist
            hierbei der Projektleiter. Dies bezieht sich aber nicht auf die
            Erstellung des Produkts, sondern auf seine Verantwortung dafür,
            dass Besprechungsdokumente für die laut Projekthandbuch zu
            dokumentierenden Besprechungen erstellt werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-besprechungsdokument.einladung">
      <resource-bundle>
        <l10n locale="de">
          <name>Einladung</name>
          <description>
            Die Einladung enthält alle im Vorfeld notwendigen Informationen zur
            Durchführung der Besprechung wie Termin, Ort, Ziel und Agenda der
            Besprechung.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-besprechungsdokument.protokoll">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
  <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.vmodellxt-besprechungsdokument.protokoll"/></ac:parameter>
  <ac:rich-text-body>
    <p>Die Ergebnisse der Besprechung.</p>
    <table class="relative-table wrapped" style="width: 100%;">
      <colgroup>
        <col style="width: 12pt; white-space: nowrap;"/>
        <col style="width: 10pt; white-space: nowrap;"/>
        <col/>
        <col style="width: 10%;"/>
        <col style="width: 10%; white-space: nowrap;"/>
      </colgroup>
      <tbody>
        <tr>
          <th style="width: 12pt; white-space: nowrap; text-align: right;">Pos</th>
          <th style="width: 10pt; white-space: nowrap; text-align: center;">Art</th>
          <th>Inhalt</th>
          <th style="text-align: center;">Verantwortlich</th>
          <th style="text-align: center; white-space: nowrap;">Termin</th>
        </tr>
        <tr>
          <td style="text-align: right;">
            <br/>
          </td>
          <td style="text-align: center;">
            <br/>
          </td>
          <td>
            <br/>
          </td>
          <td>
            <br/>
          </td>
          <td colspan="1" style="text-align: center;">
            <div class="content-wrapper">
              <p>
                <br/>
              </p>
            </div>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">
            <br/>
          </td>
          <td style="text-align: center;">
            <br/>
          </td>
          <td>
            <br/>
          </td>
          <td>
            <br/>
          </td>
          <td colspan="1" style="text-align: center;">
            <br/>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">
            <br/>
          </td>
          <td style="text-align: center;">
            <br/>
          </td>
          <td>
            <br/>
          </td>
          <td>
            <br/>
          </td>
          <td colspan="1" style="text-align: center;">
            <br/>
          </td>
        </tr>
      </tbody>
    </table>
    <p>
      <br/>
    </p>
    <ac:structured-macro ac:name="projectdoc-content-marker">
      <ac:parameter ac:name="id">legende</ac:parameter>
      <ac:rich-text-body>
        <ac:structured-macro ac:name="panel">
          <ac:parameter ac:name="title">Legende: Art</ac:parameter>
          <ac:rich-text-body>
            <table class="wrapped" style="width: 100%;">
              <colgroup>
                <col style="width: 10pt; white-space: nowrap; text-align: center;"/>
                <col/>
              </colgroup>
              <tbody>
                <tr>
                  <th>A</th>
                  <td>Auftrag (verbindlich zu erledigende Aufgabe)</td>
                </tr>
                <tr>
                  <th>B</th>
                  <td>Beschluss (verbindliche Einigung zu einem Punkt)</td>
                </tr>
                <tr>
                  <th>E</th>
                  <td>Empfehlung (unverbindlicher Vorschlag, Hinweis)</td>
                </tr>
                <tr>
                  <th colspan="1">F</th>
                  <td colspan="1">Feststellung (Information, Erkenntnis, Offener Punkt)</td>
                </tr>
              </tbody>
            </table>
          </ac:rich-text-body>
        </ac:structured-macro>
      </ac:rich-text-body>
    </ac:structured-macro>
  </ac:rich-text-body>
</ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Protokoll</name>
          <description>
            Das Protokoll ist eine schriftliche Dokumentation des Verlaufs und
            der Resultate einer Besprechung. Dabei sollten insbesondere
            Teilnehmer, Verteilerliste und die vereinbarten Aufgaben,
            gegebenenfalls in Form von Arbeitsaufträgen, enthalten sein. Das
            Protokoll ist nach Fertigstellung an alle Teilnehmer und sonstige
            Betroffene zu verteilen und von diesen auf Richtigkeit zu prüfen.
          </description>
          <label key="vmodellxt.doctype.vmodellxt-besprechungsdokument.protokoll"></label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-qs-bericht" />
    <doctype-ref id="vmodellxt-kaufmännischer-projektstatusbericht" />
    <doctype-ref id="vmodellxt-projektstatusbericht" />
    <doctype-ref id="vmodellxt-projektabschlussbericht" />
    <doctype-ref id="vmodellxt-projekttagebuch" />
  </related-doctypes>

  <wizard template="minimal-params" form-id="vmodellxt-besprechungsdokument-form">
    <field template="name" />
    <field template="short-description" height="5" />
    <field><xml><![CDATA[      <div class="field-group">
          <label for="day-date">{getText('projectdoc.blueprint.form.label.date')}</label>
          <input id="day-date" class="aui-date-picker text" type="date" name="day-date" size="10" autocomplete="off">
      </div>]]></xml></field>
    <field template="target-location" />
    <param name="projectdoc-adjustVarValues-toDatePicker">day-date</param>
  </wizard>
</doctype>
