<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-problemmeldung"
  base-template="standard"
  provide-type="standard-type"
  category="problem-und-änderungsmanagement">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Problemmeldungen">Problemmeldung</name>
      <description>
        Problemmeldungen sind der dokumentierte Wunsch nach Behebung eines
        Problems.
      </description>
      <about>
        Problemmeldungen sind der dokumentierte Wunsch nach Behebung eines
        Problems. Auslöser von Problemmeldungen können unterschiedlicher Natur
        sein, zum Beispiel Änderungen von Anforderungen oder Fehler im System.
      </about>
      <type plural="Problemmeldungstypen">Problemmeldungstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.id">
      <resource-bundle>
        <l10n locale="de">
          <name>ID</name>
          <description>
            Eindeutiger Identifikator für die Problemmeldung.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.reference">
      <resource-bundle>
        <l10n locale="de">
          <name>Referenz</name>
          <description>
            Verweis auf eine Problemmeldung in einem anderen Informationssystem.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-problemmeldung-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.datum">
      <resource-bundle>
        <l10n locale="de">
          <name>Datum</name>
          <description>
            Datum an dem die Problemmeldung erstellt wurde.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.schweregrad">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-schweregrad</param>
          <param
            name="property"
            key="vmodellxt.doctype.vmodellxt-problemmeldung.schweregrad" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Schweregrad</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.priorität">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-priorität</param>
          <param
            name="property"
            key="vmodellxt.doctype.vmodellxt-problemmeldung.priorität" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Priorität</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.element">
      <resource-bundle>
        <l10n locale="de">
          <name>Element</name>
          <description>
            Referenz zu einer Beschreibung des Systems oder Teil eines Systems
            in dem das Problem entdeckt wurde.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.protkoll">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param name="parent-doctype">#ANY</param>
          <param
            name="property-name"
            key="projectdoc.doctype.common.name" />
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Protokoll</name>
          <description>
            Referenz auf ein Protokoll, in dem der Prüfvorgang beschrieben wird,
            der zu dieser Problemmeldung führt.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-problemmeldung.verwandte-problemmeldungen">
      <resource-bundle>
        <l10n locale="de">
          <name>Verwandte Problemmeldungen</name>
          <description>
            Verweis auf Problemmeldungen, die für diese Problemmeldung relevant
            sind.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.sortKey">
      <value>
        <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-transclusion-property-display">
    <ac:parameter ac:name="property-name"><at:i18n at:key="vmodellxt.doctype.vmodellxt-problemmeldung.id"/></ac:parameter>
  </ac:structured-macro>]]></xml>
      </value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Problemmeldungen sind der dokumentierte Wunsch nach Behebung eines
            Problems. Auslöser von Problemmeldungen können unterschiedlicher
            Natur sein, zum Beispiel Änderungen von Anforderungen oder Fehler
            im System.

            Jeder Projektbeteiligte, zum Beispiel SW-Entwickler oder Anwender,
            kann eine Problemmeldung oder einen Änderungsantrag erstellen.
            Problemmeldung und Änderungsantrag können als externes Produkt auch
            von außerhalb des Projekts eingehen. Wann Problemmeldungen und
            Änderungsanträge erstellt werden müssen, um eine Änderung
            einzusteuern und durchzusetzen, ist im Projekthandbuch geregelt.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-problemmeldung.identifikation">
      <resource-bundle>
        <l10n locale="de">
          <name>Identifikation und Einordnung</name>
          <description>
            In diesem Thema werden das identifizierte Problem und der
            Änderungswunsch näher beschrieben. Dabei sind alle Informationen
            (wie eindeutige Identifikation des Problemgegenstandes,
            Antragsteller und Dringlichkeit) die notwendig sind, um das Problem
            zu reproduzieren beziehungsweise den Änderungswunsch
            nachzuvollziehen, zu dokumentieren. Jeder Änderungswunsch ist zu
            kategorisieren und einzuordnen, zum Beispiel bezüglich seiner
            Änderungsart, Änderungspriorität und Fertigstellung.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-problemmeldung.chancenbeschreibung">
      <resource-bundle>
        <l10n locale="de">
          <name>Chancenbeschreibung</name>
          <description>
            Ausgehend von der Beschreibung des Ist-Zustandes im vorhergehenden
            Thema wird in der Chancen-/Problembeschreibung der Änderungsgrund,
            zum Beispiel technische Probleme, Ressourcenknappheit und
            organisatorische Konflikte, dargelegt. In der Begründung kann auch
            auf Chancen und Nutzen der gewünschten Änderung sowie auf den
            möglichen Schaden durch eine Nicht-Durchführung der Änderungen
            hingewiesen werden.

            Bezieht sich der Antrag auf eine Abweichung des Systemverhaltens
            von den vorgegebenen Anforderungen oder auf die Änderung einer
            Anforderung, so ist diese Anforderung anzugeben.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-problemmeldung.lösungsvorschlag">
      <resource-bundle>
        <l10n locale="de">
          <name>Lösungsvorschlag</name>
          <description>
            Falls der Antragsteller konkrete Vorstellungen von der Umsetzung
            des Soll-Zustandes hat, sind diese darzustellen. Dabei sollten auch
            die Auswirkungen der Umsetzungen mit dargestellt werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-änderungsantrag" />
    <doctype-ref id="vmodellxt-änderungsbewertung" />
    <doctype-ref id="vmodellxt-problembewertung" />
    <doctype-ref id="vmodellxt-änderungsentscheidung" />
  </related-doctypes>
</doctype>
