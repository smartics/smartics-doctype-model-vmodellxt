<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-vertrag"
  base-template="standard"
  provide-type="standard-type"
  category="ausschreibungs-und-vertragswesen">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Verträge">Vertrag</name>
      <description>
        Vertragsinformationen gemäß V-Modell XT.
      </description>
      <about>
        Der Vertrag bildet die rechtliche Grundlage für die Erbringung der
        Leistungen von Auftragnehmer und Auftraggeber und regelt die
        Zusammenarbeit zwischen ihnen. Für öffentliche Auftraggeber gibt es
        vorgefertigte Vertragsbedingungen, zum Beispiel EVB-IT beziehungsweise
        BVB, die entsprechend zu verwenden und gegebenenfalls auszugestalten
        sind. Bei öffentlichen Ausschreibungen kann der Vertrag auch nur aus
        der Ausschreibung und dem ausgewählten Angebot bestehen.
      </about>
      <type plural="Vertragstypen">Vertragstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-vertrag-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-vertrag.auftraggeber">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">organization</param>
          <param name="property" key="vmodellxt.doctype.vmodellxt-vertrag.auftraggeber" />
          <param name="empty-as-none">true</param>
          <param name="render-no-hits-as-blank">true</param>
          <param name="render-list-as-comma-separated-values">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Auftraggeber</name>
          <description>
            Dokumentieren Sie den Auftraggeber.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-vertrag.auftragnehmer">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">organization</param>
          <param name="property" key="vmodellxt.doctype.vmodellxt-vertrag.auftragnehmer" />
          <param name="empty-as-none">true</param>
          <param name="render-no-hits-as-blank">true</param>
          <param name="render-list-as-comma-separated-values">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Auftragnehmer</name>
          <description>
            Dokumentieren Sie den oder die Auftragnehmer.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Beschreiben Sie kurz die wesentlichen Punkte des Vertrags.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.common.einleitung">
      <resource-bundle>
        <l10n locale="de">
          <description>
            Der Vertrag bildet die rechtliche Grundlage für die Erbringung der
            Leistungen von Auftragnehmer und Auftraggeber und regelt die
            Zusammenarbeit zwischen ihnen. Für öffentliche Auftraggeber gibt es
            vorgefertigte Vertragsbedingungen, zum Beispiel EVB-IT beziehungsweise
            BVB, die entsprechend zu verwenden und gegebenenfalls auszugestalten
            sind. Bei öffentlichen Ausschreibungen kann der Vertrag auch nur aus
            der Ausschreibung und dem ausgewählten Angebot bestehen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-vertrag.allgemeiner-vertragsteil">
      <resource-bundle>
        <l10n locale="de">
          <name>Allgemeiner Vertragsteil</name>
          <description>
            Der allgemeine Vertragsteil enthält neben einer Einleitung alle für
            den Auftraggeber und Auftragnehmer notwendigen Randinformationen,
            z.B. Hinweise auf Anlagen wie ein Organisationsprofil mit
            Referenzen und Mitarbeiterqualifikationen, eine Beschreibung des
            Qualitätsmanagementsystems, Organisationsbroschüren, relevante
            Datenblätter und Zertifikate. Oft ist an dieser Stelle auch eine
            Zusammenfassung für das Management enthalten.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-vertrag.rechtlicher-und-kommerzieller-vertragsteil">
      <resource-bundle>
        <l10n locale="de">
          <name>Rechtlicher und kommerzieller Vertragsteil</name>
          <description>
            Der rechtliche und kommerzielle Vertragsteil enthält zum einen die
            rechtlichen Bedingungen. Beispiele hierfür sind allgemeine
            Geschäftsbedingungen bzw. beim öffentlichen Auftraggeber Regelungen
            wie EVB-IT, BVB und VOL, Garantie- und Gewährleistungsbedingungen,
            Lizenzvereinbarungen, Bestimmungen für den Eigentumsübergang,
            Hinweise zu Gefahren, Vorgaben zum Preisrecht sowie der
            Gerichtsstand.

            Zum anderen sind kommerzielle Bedingungen wie beispielsweise
            Vorgaben zum Preistyp und Preisstand, zu Zahlungsbedingungen und
            -terminen sowie eine Preiskalkulation enthalten.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-vertrag.leistungsbeschreibung">
      <xml><![CDATA[]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Leistungsbeschreibung</name>
          <description>
            Die Leistungsbeschreibung enthält Informationen zu Anforderungen
            an das zu erstellende System, vertragsrelevanten Teilen des
            Projekthandbuchs und zu vertragsrelevanten Teilen des QS-Handbuchs.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-vertrag.lastenhefte">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.vmodellxt-vertrag.lastenhefte"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">false</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-lastenheft</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.lastenheft.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-lastenheft</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>+, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">lastenhefte-table, display-table, lastenhefte</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Lastenhefte</name>
          <label key="vmodellxt.label.lastenheft.create">Neues Lastenheft</label>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-vertrag.pflichtenhefte">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.vmodellxt-vertrag.pflichtenhefte"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">false</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-pflichtenheft</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.pflichtenheft.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-pflichtenheft</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>+, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">pflichtenhefte-table, display-table, pflichtenhefte</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Pflichtenhefte</name>
          <label key="vmodellxt.label.pflichtenheft.create">Neues Pflichtenheft</label>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vertrag.lieferungen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.vertrag.lieferungen"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">false</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                  <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-lieferung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.lieferung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-lieferung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-lieferung.datum"/>, <at:i18n at:key="projectdoc.doctype.common.shortName"/>+, <at:i18n at:key="vmodellxt.doctype.vmodellxt-lieferung.lieferant"/>|</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortName"/>=<at:i18n at:key="projectdoc.vmodellxt-lieferung.template.title"/></ac:parameter>
            <ac:parameter ac:name="render-classes">lieferungen-table, display-table, lieferungen</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Lieferungen</name>
          <description></description>
          <label key="vmodellxt.label.lieferung.create">Neue Lieferung</label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-lastenheft" />
    <doctype-ref id="vmodellxt-pflichtenheft" />
  </related-doctypes>
</doctype>
