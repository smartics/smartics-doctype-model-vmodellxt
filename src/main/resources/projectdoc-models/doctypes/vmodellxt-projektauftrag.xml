<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-projektauftrag"
  base-template="standard"
  provide-type="standard-type"
  category="anbahnung-und-organisation">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Projektaufträge">Projektauftrag</name>
      <description>
        Der Projektauftrag ist das zentrale Produkt zur Genehmigung eines
        Projekts und in diesem Sinne für jedes Projekt zu erstellen.
      </description>
      <about>
        Der Projektauftrag ist das zentrale Produkt zur Genehmigung eines
        Projekts und in diesem Sinne für jedes Projekt zu erstellen. Durch ihn
        wird das Projekt formal eingerichtet. Der Projektauftrag definiert, was
        im Projekt getan werden soll, wer beteiligt ist und wie vorgegangen
        werden soll.
      </about>
      <type plural="Projektauftragstypen">Projektauftragstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-projekthandbuch-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Der Projektauftrag ist das zentrale Produkt zur Genehmigung eines
            Projekts und in diesem Sinne für jedes Projekt zu erstellen. Durch ihn
            wird das Projekt formal eingerichtet. Der Projektauftrag definiert, was
            im Projekt getan werden soll, wer beteiligt ist und wie vorgegangen
            werden soll. Dazu legt der Projektauftrag bereits die grobe
            Projektorganisation fest, besetzt die zentralen Projektrollen und
            skizziert den Projektplan, soweit es zu Projektbeginn schon möglich und
            sinnvoll ist. Er beschreibt außerdem, warum das Projekt nützlich und
            wirtschaftlich ist und zählt mögliche Risiken sowie Chancen auf, die
            den Projektverlauf negativ oder positiv beeinflussen können.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-projektauftrag.projektcharta">
      <resource-bundle>
        <l10n locale="de">
          <name>Projektcharta</name>
          <description>
            Die Projektcharta ist ein Management Summary des Projektauftrags
            und stellt das Projekt überblicksartig auf maximal einer Seite dar.
            Die Projektcharta sollte Führungskräfte in die Lage versetzen, sich
            ein Bild des Projekts zu machen, ohne den Projektauftrag im Detail
            studieren zu müssen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-projektauftrag.projektmotivation-und-projektziele">
      <resource-bundle>
        <l10n locale="de">
          <name>Projektmotivation und Projektziele</name>
          <description>
            Dieses Thema beantwortet, warum das Projekt durchgeführt wird und
            welche Ziele im Projekt erreicht werden sollen. Die Projektziele
            können aus dem Projektvorschlag abgeleitet bzw. übernommen werden.
            Im Projektverlauf werden die Inhalte dieses Themas ins
            Projekthandbuch übernommen und dort gepflegt.

            Als Motivation für ein Projekt kommen beispielsweise politische
            Entscheidungen (z.B. Gesetzesänderungen), wirtschaftliche
            Betrachtungen (z.B. Reduzierung des Bearbeitungsaufwands) oder
            technische Erfordernisse (z.B. Ablösung eines Altsystems) in
            Betracht. Die Projektmotivation soll also den Sinn der
            Projektdurchführung vermitteln. Die Projektziele sollten nach dem
            SMART-Prinzip spezifisch, messbar, akzeptiert, realisierbar und
            terminierbar formuliert sein. Ggf. können auch Nicht-Ziele benannt
            werden, um das Projekt abzugrenzen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-projektauftrag.rahmenbedingungen">
      <resource-bundle>
        <l10n locale="de">
          <name>Stakeholder-Übersicht und Rahmenbedingungen</name>
          <description>
            Dieses Thema beschreibt das Projektumfeld, das im Projekt
            berücksichtigt werden muss. Dazu zählen insbesondere die
            Projektstakeholder, also alle relevanten Personen(kreise), die ein
            Interesse an dem Gelingen bzw. Scheitern des Projekts haben. Diese
            werden bereits hier überblicksartig dargestellt und im Laufe des
            Projekts genau analysiert. Aber auch unveränderliche (oder schwer
            veränderliche) Rahmenbedingungen wie zu beachtende Gesetze und
            Vorschriften, bestehende Haushaltspläne, die Einbettung in ein
            organisationsweites Berichtswesen oder technische Vorgaben sind
            hier als Rahmenbedingungen zu benennen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-projektauftrag.projektorganisation-und-projektplan">
      <resource-bundle>
        <l10n locale="de">
          <name>Projektorganisation und Projektplan</name>
          <description>
            Dieses Thema gibt einen groben Überblick über die Organisation des
            Projekts. Bereits hier sollte festgelegt werden, wie die zentralen
            Rollen Projektmanager und Projektleiter besetzt sind. Ggf. kann
            auch bereits die Zusammensetzung des Lenkungsausschusses bestimmt
            werden.

            Außerdem enthält das Thema den aktuellen (eher groben)
            Planungsstand, der im Projekt in den Projektplan übernommen und
            dort weiter detailliert und gepflegt wird. Prinzipiell können alle
            Themen des Projektplans bereits hier umrissen werden,
            beispielsweise wichtige Meilensteine, geplante Arbeitspakete,
            verfügbare Ressourcen oder geplante Budgets.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-projektauftrag.kosten-nutzen-analyse">
      <resource-bundle>
        <l10n locale="de">
          <name>Kosten-Nutzen-Analyse</name>
          <description>
            Dieses Thema beschreibt, welcher Nutzen durch die
            Projektdurchführung erzielt werden kann bzw. warum das Projekt
            wirtschaftlich ist, sich also "rechnet"; im Englischen wird häufig
            der Begriff Business Case verwendet.

            Abhängig von der Projektkonstellation dienen als Basis die
            Beschreibungen im Projektvorschlag (Thema Wirtschaftlichkeit) bzw.
            in der Bewertung der Ausschreibung (Thema
            Wirtschaftlichkeitsbetrachtung).
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-projektauftrag.chancen-und-risiken">
      <resource-bundle>
        <l10n locale="de">
          <name>Chancen und Risiken</name>
          <description>
            Ausgangspunkt für den Projektauftrag und die Projektplanung ist der
            erwartete bzw. wahrscheinliche Projektverlauf. Chancen und Risiken
            beschreiben darin nicht berücksichtigte glückliche Umstände und
            ungünstige Ereignisse, die diesen Verlauf positiv bzw. negativ
            beeinflussen können. Sie müssen im Projektverlauf ständig
            beobachtet werden, um die negativen Auswirkungen zu vermeiden
            (Risikomanagement) oder die möglichen positiven Auswirkungen auch
            tatsächlich "mitzunehmen".

            Abhängig von der Projektkonstellation dienen als Basis die
            Beschreibungen im Projektvorschlag (Thema Chancen und Risiken) bzw.
            in der Bewertung der Ausschreibung (Thema Erfolgsstrategie).
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-projektvorschlag" />
    <doctype-ref id="vmodellxt-projekthandbuch" />
  </related-doctypes>
</doctype>
