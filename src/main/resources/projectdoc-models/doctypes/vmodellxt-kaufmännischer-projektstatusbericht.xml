<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-kaufmännischer-projektstatusbericht"
  base-template="standard"
  provide-type="standard-type"
  category="berichtswesen"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocTimedTitleContextProvider">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Kaufmännische Projektstatusberichte">Kaufmännischer Projektstatusbericht</name>
      <description>
        Verfolgung geplanten Lebenszykluskosten.
      </description>
      <about>
        Der Kaufmännischer Projektstatusbericht dient zur Verfolgung der im
        Dokument Kaufmännische Projektkalkulation geplanten Lebenszykluskosten
        sowie des monetären Projektergebnisses und damit zur Steuerung der
        Wirtschaftlichkeit. Durch den kaufmännischen Projektstatusbericht
        werden zumindest der Projektleiter, der Projektmanager und der
        Lenkungsausschuss über die kaufmännische Lage des Projektes informiert.
      </about>
      <type plural="Kaufmännische Projektstatusberichtstypen">Kaufmännischer Projektstatusberichtstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-projekthandbuch-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt-kaufmännischer-projektstatusbericht.datum">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n locale="de">
          <name>Datum</name>
          <description>
            Das Datum an dem der Bericht veröffentlicht wurde.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt-kaufmännischer-projektstatusbericht.letzter-bericht">
      <resource-bundle>
        <l10n locale="de">
          <name>Letzter Bericht</name>
          <description>
            Fügen Sie einen Verweis auf den letzten kaufmännischen
            Projektstatusbericht ein.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.common.sortKey">
      <value><xml><![CDATA[<at:var at:name="day-date" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Der Kaufmännischer Projektstatusbericht dient zur Verfolgung der im
            Dokument Kaufmännische Projektkalkulation geplanten Lebenszykluskosten
            sowie des monetären Projektergebnisses und damit zur Steuerung der
            Wirtschaftlichkeit. Durch den kaufmännischen Projektstatusbericht
            werden zumindest der Projektleiter, der Projektmanager und der
            Lenkungsausschuss über die kaufmännische Lage des Projektes informiert.
            Die Anzahl und Häufigkeit der zu erstellenden Produkte Kaufmännischer
            Projektstatusbericht ist im Projekthandbuch vorgegeben.

            Die detaillierten Kostenbetrachtungen können in einigen Teilen
            vertraulich sein. Sie werden daher verdichtet in die Analyse der
            Planabweichungen des Projekttagebuchs aufgenommen und in den Kostenteil
            des Projektstatusberichts übernommen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-kaufmännischer-projektstatusbericht.planungskosten">
      <resource-bundle>
        <l10n locale="de">
          <name>Abweichungen der Planungskosten</name>
          <description>
            Die Planungskosten werden den im Dokument Kaufmännische
            Projektkalkulation geplanten Soll-Kosten gegenüber gestellt.
            Etwaige Abweichungen werden dokumentiert sowie inhaltlich
            analysiert.

            Aus den Abweichungen können Vorschläge für Änderungen abgeleitet
            werden, um die im Dokument Kaufmännische Projektkalkulation
            geplanten Planungskosten einzuhalten.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-kaufmännischer-projektstatusbericht.projektkosten">
      <resource-bundle>
        <l10n locale="de">
          <name>Abweichungen der Projektkosten</name>
          <description>
            Die im Projekt angefallenen Ist-Kosten werden unter
            Berücksichtigung zusätzlicher Kostenfaktoren, wie z.B. Gemeinkosten
            und Zinsbelastungen, den im Produkt Kaufmännische
            Projektkalkulation geplanten Soll-Kosten gegenübergestellt. Etwaige
            Abweichungen werden dokumentiert sowie inhaltlich analysiert.

            Abhängig vom Fertigstellungsgrad werden die noch bis zum
            Projektende zu erwartenden Kosten, wie z.B. Personalkosten,
            Materialkosten und Reisekosten, ermittelt (Cost to Complete). Dabei
            werden zusätzliche Kosten wie beispielsweise Risikozuschläge,
            Zinsen und Finanzierungskosten mit eingerechnet. Aus diesen Daten
            können die Gesamtkosten bei Projektende (Cost at Completion)
            abgeleitet werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-kaufmännischer-projektstatusbericht.herstellkosten">
      <resource-bundle>
        <l10n locale="de">
          <name>Abweichungen der Herstellkosten</name>
          <description>
            Die später in der Fertigung anfallenden Herstellkosten werden auf
            Basis der aktuellen Informationen neu kalkuliert und den im Produkt
            Kaufmännische Projektkalkulation geplanten Soll-Kosten
            gegenübergestellt. Etwaige Abweichungen werden dokumentiert sowie
            inhaltlich analysiert.

            Die Herstellkosten werden dabei so weit detailliert, dass
            Kostentreiber bei einzelnen Systemelementen erkennbar sind. Aus
            Abweichungen können Vorschläge für technische Änderungen abgeleitet
            werden, um die im Produkt Kaufmännische Projektkalkulation
            geplanten Herstellkosten einzuhalten.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-kaufmännischer-projektstatusbericht.nutzungskosten">
      <resource-bundle>
        <l10n locale="de">
          <name>Abweichungen der Nutzungskosten</name>
          <description>
            Die voraussichtlichen Nutzungskosten werden den im Produkt
            Kaufmännische Projektkalkulation geplanten Soll-Kosten gegenüber
            gestellt. Etwaige Abweichungen werden dokumentiert sowie inhaltlich
            analysiert.

            Aus den voraussichtlichen Abweichungen können Vorschläge für
            technische Änderungen abgeleitet werden, um die im Produkt
            Kaufmännische Projektkalkulation geplanten Nutzungskosten
            einzuhalten.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-kaufmännischer-projektstatusbericht.wirtschaftlichkeit">
      <resource-bundle>
        <l10n locale="de">
          <name>Abweichungen der Wirtschaftlichkeit</name>
          <description>
            Das erwartete Ergebnis wird auf Basis der aktuellen Informationen
            neu kalkuliert und dem im Produkt Kaufmännische Projektkalkulation
            geplanten Soll-Ergebnis gegenübergestellt. Etwaige Abweichungen
            werden dokumentiert sowie inhaltlich analysiert.

            Positive und negative Wirkungen von Abweichungen gegenüber den
            Planwerten müssen einander kompensieren. Sollte das geplante
            Ergebnis nicht erreicht werden können bzw. die Wirtschaftlichkeit
            nicht gegeben sein, müssen steuernde Maßnahmen vorgeschlagen und
            ergriffen werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-qs-bericht" />
    <doctype-ref id="vmodellxt-projektstatusbericht" />
    <doctype-ref id="vmodellxt-projektabschlussbericht" />
    <doctype-ref id="vmodellxt-besprechungsdokument" />
    <doctype-ref id="vmodellxt-projekttagebuch" />
  </related-doctypes>
</doctype>
