<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-abnahmespezifikation"
  base-template="standard"
  provide-type="standard-type"
  category="qs"
  has-homepage="false"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSubdocumentContextProvider">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Abnahmespezifikationen">Abnahmespezifikation</name>
      <description>
        Spezifizieren Sie die Abnahme einer Lieferung nach dem V-Modell XT.
      </description>
      <about>
        Für jede Lieferung muss eine Abnahmeprüfung durchgeführt werden. Die
        Abnahmespezifikation ist die Grundlage für diese Abnahmeprüfung. In ihr
        werden alle zur Abnahme notwendigen Prüffälle und - falls die Lieferung
        auch Dokumente enthält - auch die notwendigen Prüfkriterien definiert.
      </about>
      <type plural="Abnahmespezifikationstypen">Abnahmespezifikationstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-abnahmespezifikation-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.lieferung">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param
            name="property-name"
            key="projectdoc.doctype.common.name" />
          <param
            name="property"
            key="projectdoc.doctype.common.parent" />
          <param name="parent-doctype">vmodellxt-lieferung</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Lieferung</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.übergeordnete-spezifikation">
      <value>
        <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-transclusion-parent-property">
                      <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
                      <ac:parameter ac:name="parent-doctype">vmodellxt-abnahmespezifikation</ac:parameter>
                    </ac:structured-macro>]]></xml>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Übergeordnete Spezifikation</name>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Für jede Lieferung muss eine Abnahmeprüfung durchgeführt werden.
            Die Abnahmespezifikation ist die Grundlage für diese
            Abnahmeprüfung. In ihr werden alle zur Abnahme notwendigen
            Prüffälle und - falls die Lieferung auch Dokumente enthält - auch
            die notwendigen Prüfkriterien definiert.

            Sie enthält die Spezifikation der Eingangskontrolle einschließlich
            der Überprüfung der Sollkonfiguration. Die Sollkonfiguration wird
            entweder vom Auftraggeber vorgeschrieben oder ist in der Lieferung
            enthalten, zum Beispiel in den Release Notes. Darüber hinaus
            enthält die Abnahmespezifikation alle zur Abnahmeprüfung
            notwendigen Prüffälle sowie die Prüfumgebung. Sie wird aus den im
            Vertrag und in den Vertragszusätzen enthaltenen Anforderungen - und
            nur aus diesen - erstellt. Die Abdeckung dieser Anforderungen an
            die Lieferung durch die Prüffälle und Prüfkriterien ist zu
            dokumentieren, beispielsweise in Form einer Abdeckungsmatrix.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüfobjekt">
      <resource-bundle>
        <l10n locale="de">
          <name>Prüfobjekt</name>
          <description>
            Es ist die eindeutig definierte identifizierbare Version des
            Prüfobjektes festzulegen, auf die sich die Prüfspezifikation
            beziehungsweise das Prüfprotokoll bezieht.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüfstrategie">
      <resource-bundle>
        <l10n locale="de">
          <name>Prüfstrategie</name>
          <description>
            Die Prüfstrategie beschreibt, wie die Anforderungen an das
            Prüfobjekt durch eine geeignete Struktur von Prüffällen in der
            notwendigen und geforderten Prüfungstiefe abgeprüft werden können.
            Dabei werden die verwendeten Prüfmethoden, wie zum Beispiel
            Funktionsprüfung und Stressprüfung, und Nachweismethoden, wie zum
            Beispiel Test, Nachweis und Demonstrator, festgelegt.

            Die anzuwendende Prüfstrategie wird aus dem entsprechenden
            Implementierungs-, Integrations- und Prüfkonzept abgeleitet und
            gegebenenfalls angemessen verfeinert.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüffälle">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüffälle"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                  <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfprozedur-systemelement</ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfprozedur-systemelement.create.label"/></ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfprozedur-systemelement</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">prüffälle-table, display-table, prüffälle</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
  </ac:rich-text-body>
</ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Prüffälle</name>
          <description>
            Basierend auf der Konzeption der Prüfstrategie erfolgt in diesem
            Thema eine Beschreibung der einzelnen Prüffälle mit den hierfür
            notwendigen Informationen wie Startzustand des Systems, Prüfablauf
            und erwarteter Endzustand des Systems.

            Besonders zu berücksichtigen sind der Abdeckungsgrad der Prüffälle
            sowie die Endekriterien. Der Abdeckungsgrad legt fest, wie
            detailliert zu prüfen ist. Die Endekriterien benennen Bedingungen,
            unter denen die Prüfung erfolgreich abgeschlossen ist.
          </description>
          <label key="vmodellxt.doctype.vmodellxt-prüfprozedur-systemelement.create.label">Neuer Prüffall</label>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüfkriterien">
      <resource-bundle>
        <l10n locale="de">
          <name>Prüfkriterien</name>
          <description>
            In den Prüfkriterien wird die Prüfmethode (beispielsweise Review,
            Inspektion und Interviews), der Abdeckungsgrad (zum Beispiel
            Stichprobenprüfung und vollständige Prüfung) sowie die formalen und
            inhaltlichen Prüfkriterien (wie inhaltliche Korrektheit, Einhaltung
            der Projektstandards, Gestaltung, Rechtschreibung) beschrieben. Zu
            den Prüfkriterien gehören auch die Bedingungen für das erfolgreiche
            beziehungsweise nicht erfolgreiche Ende der Prüfung.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüfumgebung">
      <resource-bundle>
        <l10n locale="de">
          <name>Prüfumgebung</name>
          <description>
            Die allgemeine Prüfumgebung wird bereits in den zugehörigen
            Implementierungs-, Integrations- und Prüfkonzepten beschrieben. In
            diesem Thema werden notwendige Ausgestaltungen und Erweiterungen
            der allgemeinen Prüfumgebung oder speziell notwendige
            Prüfumgebungen für das konkrete Prüfobjekt beschrieben, wie zum
            Beispiel ein Drehtisch mit Echtzeitbildsimulation für einen
            Flugkörper oder eine Autoteststrecke mit einem entsprechenden
            Fahrparcours.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.prüffallzuordnung">
      <resource-bundle>
        <l10n locale="de">
          <name>Prüffallzuordnung</name>
          <description>
            Die aus den Anforderungen abgeleiteten Prüffälle werden den
            Anforderungen zugeordnet. Das erfolgt beispielsweise mithilfe einer
            Abdeckungsmatrix. Hier soll sichtbar werden, ob der gewünschte
            Abdeckungsgrad und die Prüfqualität gegeben sind, besonders in
            Bezug auf die vorher festgelegte Prüfstrategie.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.schutzvorkehrungen">
      <resource-bundle>
        <l10n locale="de">
          <name>Schutzvorkehrungen</name>
          <description>
            Für jedes Prüfobjekt, das ein Gefährdungspotential bei der Prüfung
            hat und damit nicht normal getestet werden kann, wird beschrieben,
            welche Vorkehrungen und Maßnahmen durchzuführen sind, damit bei
            seiner Prüfung keine Gefährdungen auftreten können.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.abnahmeprotokolle">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.vmodellxt-abnahmespezifikation.abnahmeprotokolle"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-abnahmeprotokoll</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.common.bearbeitungszustand"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-abnahmeprotokoll.prüfdatum"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-abnahmeprotokoll.prüfergebnis"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="vmodellxt.doctype.vmodellxt-abnahmeprotokoll.spezifikation"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}]</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">abnahmeprotokolle-table, display-table, abnahmeprotokolle</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
  </ac:rich-text-body>
</ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n locale="de">
          <name>Abnahmeprotokolle</name>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-lieferung" />
    <doctype-ref id="vmodellxt-liefergegenstand" />
    <doctype-ref id="vmodellxt-abnahmeprotokoll" />
    <doctype-ref id="vmodellxt-abnahmeerklärung" />
    <doctype-ref id="vmodellxt-prüfspezifikation" />
    <doctype-ref id="vmodellxt-prüfprotokoll" />
    <doctype-ref id="vmodellxt-prüfspezifikation-systemelement" />
    <doctype-ref id="vmodellxt-prüfprozedur-systemelement" />
    <doctype-ref id="vmodellxt-prüfprotokoll-systemelement" />
  </related-doctypes>

  <wizard template="no-homepage">
    <param name="adjust-title-on-any-parent">true</param>
  </wizard>
</doctype>
