<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-änderung"
  base-template="standard"
  provide-type="standard-type"
  has-homepage="false"
  category="metadaten"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSubdocumentContextProvider">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Änderungen">Änderung</name>
      <description>
        Dokumentieren Sie Änderungen an einem Dokument.
      </description>
      <about>
        Notieren Sie Metadaten und Informationen zu einer Änderung an einem
        Dokument.
      </about>
      <type plural="Änderungstypen">Änderungstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.name">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_common_name"/>]]></xml></value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.common.parent">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param
            name="property-name"
            key="projectdoc.doctype.common.name" />
          <param
            name="property"
            key="projectdoc.doctype.common.parent" />
          <param name="parent-doctype">#ANY</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n locale="de">
          <name>Dokument</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-änderung-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-änderung.autoren">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param name="property" key="vmodellxt.doctype.vmodellxt-änderung.autoren" />
          <param name="empty-as-none">true</param>
          <param name="render-no-hits-as-blank">true</param>
          <param name="render-list-as-comma-separated-values">true</param>
        </macro>
      </value>
      <controls>mandatory</controls>
      <resource-bundle>
        <l10n locale="de">
          <name>Autoren</name>
          <description>
            Nennen Sie die Stakeholder, die für die Änderung verantwortlich
            sind.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-änderung.datum">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <controls>mandatory</controls>
      <resource-bundle>
        <l10n locale="de">
          <name>Datum</name>
          <description>
            Notieren Sie das Datum der Änderung.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-änderung.version">
      <controls>mandatory</controls>
      <resource-bundle>
        <l10n locale="de">
          <name>Version</name>
          <description>
            Spezifizieren Sie die neue Version des Dokuments, die durch diese
            Änderung erreicht wurde.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-änderung.kapitel">
      <controls>mandatory</controls>
      <resource-bundle>
        <l10n locale="de">
          <name>Kapitel</name>
          <description>
            Listen Sie die Kapitel, die durch diese Änderung betroffen sind.
            Geben Sie "Alle" an, falls Änderungen an allen Kapiteln vorgenommen
            wurden.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param name="property" key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand" />
          <param name="empty-as-none">true</param>
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <controls>mandatory</controls>
      <resource-bundle>
        <l10n locale="de">
          <name>Bearbeitungszustand</name>
          <description>
            Legen Sie den Bearbeitungszustand des Dokuments fest.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Grund und die Art der Änderung an dem Dokument.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-bearbeitungszustand" />
    <doctype-ref id="vmodellxt-prüfung" />
  </related-doctypes>

  <wizard template="minimal-params" form-id="vmodellxt-änderung-form">
    <field template="name" />
    <field><xml><![CDATA[      <div class="field-group">
          <label for="day-date">{getText('projectdoc.blueprint.form.label.date')}</label>
          <input id="day-date" class="aui-date-picker text" type="date" name="day-date" size="10" autocomplete="off">
      </div>]]></xml></field>
    <field template="short-description" />
    <param name="projectdoc-adjustVarValues-toDatePicker">day-date</param>
    <param name="adjust-title-on-any-parent">true</param>
  </wizard>
</doctype>
