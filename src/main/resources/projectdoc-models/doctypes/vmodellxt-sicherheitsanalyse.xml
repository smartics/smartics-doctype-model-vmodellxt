<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-sicherheitsanalyse"
  base-template="standard"
  provide-type="standard-type"
  category="systemanalyse">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Sicherheitsanalysen">Sicherheitsanalyse</name>
      <description>
        Risikoermittlung und Auswahl von Maßnahmen zur Risikominderung.
      </description>
      <about>
        Ziel der Sicherheitsanalyse (häufig auch Risikoanalyse genannt) ist die
        Ermittlung der Ursachen von Gefährdungen, sowie die Abschätzung der
        Wahrscheinlichkeit für den Eintritt dieser Gefährdung bzgl. der
        Funktionssicherheit.

        Die Risiken (Eintrittswahrscheinlichkeit mal Schadenshöhe je
        Gefährdung) werden ermittelt und Maßnahmen zur Risikominderung der
        Gefährdungen ausgewählt. Die Auswahl ist zu begründen.
      </about>
      <type plural="Sicherheitsanalysetypen">Sicherheitsanalysetyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-sicherheitsanalyse-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Ziel der Sicherheitsanalyse (häufig auch Risikoanalyse genannt) ist
            die Ermittlung der Ursachen von Gefährdungen, sowie die Abschätzung
            der Wahrscheinlichkeit für den Eintritt dieser Gefährdung bzgl. der
            Funktionssicherheit.

            Die Risiken (Eintrittswahrscheinlichkeit mal Schadenshöhe je
            Gefährdung) werden ermittelt und Maßnahmen zur Risikominderung der
            Gefährdungen ausgewählt. Die Auswahl ist zu begründen.

            Die Sicherheitsanalyse ist für jedes als sicherheitskritisch
            eingestufte Systemelement durchzuführen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.sicherheitsanalyse.gefährdungsidentifikation-und-schadensklassifikation">
      <resource-bundle>
        <l10n locale="de">
          <name>Gefährdungsidentifikation und Schadensklassifikation</name>
          <description>
            Die Gefährdungsidentifikation und Schadensklassifikation beschreibt Gefährdungen, die möglicherweise beim Einsatz des zu untersuchenden Systems zu Schadensereignissen führen. Für jede Gefährdung wird die potenzielle Schadenshöhe je Schadenskategorie angegeben. Für jede identifizierte Gefährdung wird die zugeordnete Schadensklasse je Schadenskategorie angegeben.

            Schadensereignisse können je nach Systemart unterschiedliche Schadenskategorien wie Tod, Verletzungen, Krankheit, den Verlust oder Beschädigungen von Gerätschaften, Eigentum und/oder Umweltschäden zur Folge haben. Es kann sich aber auch um einen reinen Vermögensschaden z.B. durch Produktionsausfall oder Nichtverfügbarkeit eines dringend benötigten Systems handeln.

            Ebenso können immaterielle Schäden verursacht werden, wie z.B. bei Verstößen gegen gesetzliche Vorgaben/Auflagen, Imageschäden als Verkaufshindernisse oder Auslöser von Rückrufaktionen. Jedes Schadensereignis, das durch eine Gefährdung eintreten kann, hat unterschiedlich schwere Folgen. Um diese leichter handhaben zu können, werden die Schadensereignisse zweckmäßigerweise in Schadensklassen eingeteilt.          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.sicherheitsanalyse.folgenanalyse-und-relevanzeinstufung">
      <resource-bundle>
        <l10n locale="de">
          <name>Folgenanalyse und Relevanzeinstufung</name>
          <description>
            Für jede Gefährdung, die in der Gefährdungsidentifikation erkannt wurde, werden folgende Ergebnisse zusammengestellt:

            Ursachen der Gefährdung,
              Eintrittswahrscheinlichkeit der Gefährdung,
              Ermittlung des Risikos (Eintrittswahrscheinlichkeit des Schadens mal Schadenshöhe)
              Feststellung, ob das ermittelte Risiko im Rahmen des vom Auftraggeber akzeptierten Risikos liegt. Ist das Risiko über dem Akzeptanzwert, sind im nächsten Schritt risikomindernde Maßnahmen auszuwählen.
              Die Eintrittswahrscheinlichkeit bei der Gefährdung "Ausfall einer Komponente" kann auf der Basis der Lebensdauer eines Systemteils oder der Betriebsstunden angegeben werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.sicherheitsanalyse.sicherheitsmaßnahmen">
      <resource-bundle>
        <l10n locale="de">
          <name>Sicherheitsmaßnahmen</name>
          <description>
            Für alle in der Sicherheitsanalyse als nicht akzeptabel bewerteten Risiken werden Maßnahmen zur Risikominderung ermittelt. Die Vorschläge risikomindernden Maßnahmen findet sich im Projekthandbuch im Thema Organisation und Vorgaben zur Sicherheit.

            Die Notwendigkeit der Maßnahmen ergibt sich aus dem Auftreten einer Gefährdung, die außerhalb des vorgegebenen Toleranzbereichs beziehungsweise jenseits des Schwellenwertes liegt und somit nicht akzeptiert wird. Deshalb ist es erforderlich, geeignete Maßnahmen zu ermitteln und zu prüfen, ob durch die Durchführung dieser Maßnahme(n) das vorliegende Risiko derart gemindert wird, dass es akzeptiert werden kann.

            Sicherheitsmaßnahmen können aus konstruktiven Verfahren (in Hinblick auf Systementwicklung und Realisierung), analytischen Maßnahmen (Prüfmaßnahmen), zusätzlichen funktionalen oder nicht-funktionalen Anforderungen an das System sowie zusätzlichen Sicherheitseinrichtungen oder organisatorischen Auflagen bestehen.

            Risikominderungsmaßnahmen sollen die Schadenshöhe (Schadensklasse) und/oder die Eintrittswahrscheinlichkeit einer Gefährdung mindern.

            Die Auswirkungen der Maßnahmen, wie Grad der Minderung, Aufwand der Umsetzung, Auswirkungen auf Inbetriebnahme, Betrieb, Stilllegung oder Bedienpersonal, werden hinsichtlich ihrer technischen und wirtschaftlichen Eignung bewertet.

            Die Entscheidung für die Auswahl der geeignetsten Maßnahmen wird begründet.

            Sollte keine geeignete Maßnahme gefunden werden, so ist nach den Vorgaben zur Sicherheit im Projekthandbuch zu verfahren. Es muss zusammen mit dem Auftraggeber eine Lösung gesucht werden und diese muss durch einen Problemmeldungs-/Änderungsantrag eingebracht und der Lösungsweg dokumentiert werden.          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-altsystemanalyse" />
    <doctype-ref id="vmodellxt-pflichtenheft" />
    <doctype-ref id="vmodellxt-lastenheft" />
    <doctype-ref id="vmodellxt-datenschutzkonzept" />
  </related-doctypes>
</doctype>
