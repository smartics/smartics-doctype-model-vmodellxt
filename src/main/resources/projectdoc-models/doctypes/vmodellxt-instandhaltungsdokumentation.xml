<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="vmodellxt-instandhaltungsdokumentation"
  base-template="standard"
  provide-type="standard-type"
  category="logistikelemente">
  <resource-bundle>
    <l10n locale="de">
      <name plural="Instandhaltungsdokumentation">Instandhaltungsdokumentation</name>
      <description>
        Beschreibt alle Maßnahmen, die notwendig sind, um die
        Funktionsfähigkeit eines Systems zu sichern und aufrechtzuerhalten.
      </description>
      <about>
        Die Instandhaltungsdokumentation beschreibt alle Maßnahmen, die
        notwendig sind, um die Funktionsfähigkeit eines Systems zu sichern und
        aufrechtzuerhalten. Die Instandhaltung findet geplant und in
        regelmäßigen Abständen statt, bei einem Fahrzeug beispielsweise jedes
        Jahr oder alle 15.000 Km. Die Instandhaltungsdokumentation richtet sich
        an Personen, die Instandhaltungen planen und durchführen.
      </about>
      <type plural="Instandhaltungsdokumentationstypen">Instandhaltungsdokumentationstyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-instandhaltungsdokumentation-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="vmodellxt.doctype.common.bearbeitungszustand">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">vmodellxt-bearbeitungszustand</param>
          <param
            name="property"
            key="vmodellxt.doctype.common.bearbeitungszustand" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n locale="de">
          <description>
            Die Instandhaltungsdokumentation beschreibt alle Maßnahmen, die
            notwendig sind, um die Funktionsfähigkeit eines Systems zu sichern
            und aufrechtzuerhalten. Die Instandhaltung findet geplant und in
            regelmäßigen Abständen statt, bei einem Fahrzeug beispielsweise
            jedes Jahr oder alle 15.000 Km. Die Instandhaltungsdokumentation
            richtet sich an Personen, die Instandhaltungen planen und
            durchführen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.common.weitereProduktinformationen">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.mitwirkend"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="vmodellxt.label.beteiligter"/></ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:rich-text-body>
              <table>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="vmodellxt.label.beteiligter"/></th>
                    <th><at:i18n at:key="vmodellxt.label.rolle"/></th>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.änderungsverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.änderungsverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-änderung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.änderung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-änderung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.version"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.kapitel"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.autoren"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-änderung.bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.änderung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="auto-number">true</ac:parameter>
            <ac:parameter ac:name="render-classes">änderungsverzeichnis-table, display-table, änderungsverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml></section>
    <section key="vmodellxt.doctype.common.prüfverzeichnis">
      <xml>
<![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="vmodellxt.doctype.common.prüfverzeichnis"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:projectdoc-blueprint-doctype-vmodellxt-prüfung</ac:parameter>
                  <ac:parameter ac:name="buttonLabel"><at:i18n at:key="vmodellxt.label.prüfung.create"/></ac:parameter>
                  <ac:parameter ac:name="createResult">edit</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">vmodellxt-prüfung</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.datum"/>+|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.version"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.prüfer"/>|, <at:i18n at:key="vmodellxt.doctype.vmodellxt-prüfung.neuer-bearbeitungszustand"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="header-translations"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/>=<at:i18n at:key="vmodellxt.label.prüfung"/></ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">prüfverzeichnis-table, display-table, prüfverzeichnis</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-instandhaltungsdokumentation.instandhaltungsplan">
      <resource-bundle>
        <l10n locale="de">
          <name>Instandhaltungsplan</name>
          <description>
            Der Instandhaltungsplan beschreibt die einzelnen
            Instandhaltungsmaßnahmen und den Turnus, in dem diese durchgeführt
            werden müssen. Dabei können die Instandhaltungsmaßnahmen in
            Instandhaltungsstufen gebündelt werden. Die Instandhaltung kann
            während des Betriebs oder im Rahmen einer Betriebsunterbrechung
            stattfinden.

            Der Instandhaltungsplan kann auch den Instandhaltungsnachweis
            enthalten, sofern für jedes System ein eigener Instandhaltungsplan
            vorhanden ist. Ist dies nicht der Fall, so ist der
            Instandhaltungsnachweis (Fristennachweis) in geeigneter Form wie
            zum Beispiel als Serviceheft, Wartungsbuch oder Lebenslaufakte zu
            führen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="vmodellxt.doctype.vmodellxt-instandhaltungsdokumentation.instandhaltungsanleitung">
      <resource-bundle>
        <l10n locale="de">
          <name>Instandhaltungsanleitung</name>
          <description>
            Die Instandhaltungsanleitung beschreibt die Durchführung der
            verschiedenen Instandhaltungsmaßnahmen in nachvollziehbaren
            Arbeitsschritten. Die Instandhaltungsanleitung wird nur für
            Maßnahmen erstellt, für die zusätzliche Erläuterungen zum
            Instandhaltungsplan erforderlich sind. Die Entsorgung von
            Verschleißteilen und Betriebsflüssigkeiten muss dabei
            berücksichtigt werden. Die Verwendung von Mess- und Prüfgeräten
            sowie von notwendigen Werkzeugen wird erläutert.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="vmodellxt-ausbildungsunterlagen" />
    <doctype-ref id="vmodellxt-nutzungsdokumentation" />
    <doctype-ref id="vmodellxt-instandsetzungsdokumentation" />
    <doctype-ref id="vmodellxt-ersatzteil" />
  </related-doctypes>
</doctype>
