require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-doctype-addon-vmodellxt:create-doctype-template-vmodellxt-prufprotokoll-systemelement', function (wizard) {
        wizard.on('pre-render.page1Id', function (e, state) {
          state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
        });

        wizard.on("post-render.page1Id", function (e, state) {
          AJS.$('#day-date').datePicker({
            overrideBrowserDefault: true,
            dateFormat: "yy-mm-dd"
          });
          AJS.$('#day-date').attr('placeholder', AJS.I18n.getText('projectdoc.vmodellxt.wizard.date.placeholder'));
          PROJECTDOC.postrenderWizard(e, state);
        });

        wizard.on('submit.page1Id', function (e, state) {
          return PROJECTDOC.validateStandardForm(e, state);
        });

        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
      });
  });
});