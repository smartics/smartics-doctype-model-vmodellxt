<?xml version='1.0'?>
<!--

    Copyright 2017-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    The V-Modell XT is protected by copyright. Copyright 2006 V-Modell XT authors
    and others. All rights reserved. Licensed under Apache License Version 2.0.

-->
<add-on
  xmlns="http://smartics.de/xsd/projectdoc/add-on/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  default-locale="de">

  <resource-bundle>
    <l10n locale="de">
      <name>V-Modell XT</name>
      <description>
        Das V-Modell XT ist ein Vorgehensmodell für die Durchführung von
        IT-Projekten, insbesondere zur Entwicklung von Softwaresystemen.
      </description>
    </l10n>
  </resource-bundle>

  <categories>
    <category id="anbahnung-und-organisation">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Anbahnung und Organisation">Anbahnung und Organisation</name>
          <description>
            Informationen zur Anbahnung und Organisation von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="ausschreibungs-und-vertragswesen">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Ausschreibungs- und Vertragswesen">Ausschreibungs- und Vertragswesen</name>
          <description>
            Informationen zum Ausschreibungs- und Vertragswesen von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="berichtswesen">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Berichtswesen">Berichtswesen</name>
          <description>
            Informationen zum Berichtswesen von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="lieferung-und-abnahme">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Lieferung und Abnahme">Lieferung und Abnahme</name>
          <description>
            Informationen zur Lieferung und Abnahmen von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="logistikelemente">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Logistikelemente">Logistikelemente</name>
          <description>
            Informationen zur Logistik von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="metadaten">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Metadaten">Metadaten</name>
          <description>
            Informationen zu Metadaten von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="planung-und-steuerung">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Planung und Steuerung">Planung und Steuerung</name>
          <description>
            Informationen zur Planung und Steuerung von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="problem-und-änderungsmanagement">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Problem- und Änderungsmanagement">Problem- und Änderungsmanagement</name>
          <description>
            Informationen zum Problem- und Änderungsmanagement von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="qs">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Qualitätssicherung">Qualitätssicherung</name>
          <description>
            Informationen zur Qualitätssicherung von IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="systemanalyse">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Systemanalyse">Systemanalyse</name>
          <description>
            Informationen zur Analyse von Systemen in IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
    <category id="systemelemente">
      <resource-bundle>
        <l10n locale="de">
          <name plural="Systemelemente">Systemelemente</name>
          <description>
            Informationen zu Elementen von Systemen in IT-Projekten.
          </description>
        </l10n>
      </resource-bundle>
    </category>
  </categories>

  <import-doctypes>
    <set
      id="de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:projectdoc-blueprint-doctype-"
      short-id="core">
      <doctype-ref id="association-type" />
      <doctype-ref id="category" />
      <doctype-ref id="category-type" />
      <doctype-ref id="charter" />
      <doctype-ref id="charter-type" />
      <doctype-ref id="docsection" />
      <doctype-ref id="docsection-type" />
      <doctype-ref id="docmodule" />
      <doctype-ref id="module-type" />
      <doctype-ref id="experience-level" />
      <doctype-ref id="experience-level-type" />
      <doctype-ref id="faq" />
      <doctype-ref id="faq-type" />
      <doctype-ref id="glossary-item" />
      <doctype-ref id="glossary-item-type" />
      <doctype-ref id="mediatype" />
      <doctype-ref id="mediatype-type" />
      <doctype-ref id="metadata" />
      <doctype-ref id="metadata-type" />
      <doctype-ref id="organization" />
      <doctype-ref id="organization-type" />
      <doctype-ref id="person" />
      <doctype-ref id="person-type" />
      <doctype-ref id="profile" />
      <doctype-ref id="profile-type" />
      <doctype-ref id="quote" />
      <doctype-ref id="quote-type" />
      <doctype-ref id="relation" />
      <doctype-ref id="resource" />
      <doctype-ref id="resource-type" />
      <doctype-ref id="role" />
      <doctype-ref id="role-type" />
      <doctype-ref id="section-type" />
      <doctype-ref id="stakeholder" />
      <doctype-ref id="stakeholder-type" />
      <doctype-ref id="step-type" />
      <doctype-ref id="subject" />
      <doctype-ref id="subject-type" />
      <doctype-ref id="tag" />
      <doctype-ref id="tag-type" />
      <doctype-ref id="topic" />
      <doctype-ref id="topic-type" />
      <doctype-ref id="tour" />
      <doctype-ref id="tour-type" />
      <doctype-ref id="version-type" />
    </set>
  </import-doctypes>
</add-on>