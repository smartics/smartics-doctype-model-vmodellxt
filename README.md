# projectdoc Doctypes for V-Modell®XT


## Overview
This project specifies a model to create a free [doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw)
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

It provides space and page blueprints to use V-Modell®XT products (templates)
in your Confluence wiki.

## Fork me!
Feel free to fork this project to adjust the model according to your project
requirements.

The Doctypes Model project and it's derived artifacts are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

V-Modell® is a registered trademark of the Federal Republic of Germany.

V-Modell® ist eine geschützte Marke der Bundesrepublik Deutschland.

Das V-Modell ® XT ist unter der Apache License Version 2.0 freigegeben.

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/x/rAH-B)
  * [V-Modell XT](https://www.cio.bund.de/Web/DE/Architekturen-und-Standards/V-Modell-XT/vmodell_xt_node.html) Information on the V-Modell®XT on cio.bund.de (German)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)
